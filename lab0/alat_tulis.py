harga_pulpen = 3000
harga_pensil = 2000
harga_cortape = 8000

print("Toko ini menjual:")
print("1. Pulpen ", str(harga_pulpen) + "/pcs")
print("2. Pensil ", str(harga_pensil) + "/pcs")
print("3. Correction Tape", str(harga_cortape) + "/pcs")

print("Masukkan jumlah barang yang ingin dibeli:")
jumlah_pulpen = int(input("Pulpen: "))
jumlah_pensil = int(input("Pensil: "))
jumlah_cortape = int(input("Correction Tape: "))

total_pulpen = jumlah_pulpen*harga_pulpen
total_pensil = jumlah_pensil*harga_pensil
total_cortape = jumlah_cortape*harga_cortape
total = total_pulpen + total_pensil + total_cortape

print("Ringkasan pembelian: ")
print(f"{jumlah_pulpen} Pulpen x {harga_pulpen}")
print(f"{jumlah_pensil} Pensil x {harga_pensil}")
print(f"{jumlah_cortape} Correction Tape x {harga_cortape}")
print("Total harga:", total)
