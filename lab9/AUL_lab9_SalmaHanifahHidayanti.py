import datetime

#superclass User yang mencakup data semua jenis role karyawan
class User :
    def __init__(self, username, password, nama, umur, role) :
        self.__username = username
        self.__password = password
        self.__nama = nama
        self.__umur = umur
        self.__role = role

    def getUsername(self) :
        return self.__username

    def getPassword(self) :
        return self.__password

    def getNama(self) :
        return self.__nama
    
    def getUmur(self) :
        return self.__umur

    def getRole(self) :
        return self.__role

#subclass untuk karyawan kasir
class Kasir(User) :
    def __init__(self, username, password, nama, umur, role, pembayaran) :
        super().__init__(username, password, nama, umur, role)
        self.__pembayaran = int(pembayaran)

    def getPembayaran(self) :
        return self.__pembayaran

    def prosesPembayaran(self, bayar) :
        self.__pembayaran += bayar
        print(f"Berhasil menerima uang sebanyak {bayar}")

#subclass untuk karyawan janitor
class Janitor(User) :
    def __init__(self, username, password, nama, umur, role, bebersih, deterjen) :
        super().__init__(username, password, nama, umur, role)
        self.__bebersih = bebersih
        self.__deterjen = int(deterjen)

    def getBebersih(self) :
        return self.__bebersih

    def getDeterjen(self) :
        return self.__deterjen

    def beliDeterjen(self, stok) :
        self.__deterjen += stok
        print(f"Berhasil membeli {stok} deterjen")

    def bebersihToko(self) :
        if self.__deterjen <= 0 :
            print("Diperlukan setidaknya 1 deterjen untuk melakukan pembersihan")
        else :
            self.__deterjen -= 1
            print("Toko berhasil dibersihkan") 

#subclass untuk karyawan chef
class Chef(User) :
    def __init__(self, username, password, nama, umur, role, kue) :
        super().__init__(username, password, nama, umur, role)
        self.__kue = kue

    def getKue(self) :
        return self.__kue

    def buatKue(self, jumlahKue) :
        self.__kue += jumlahKue
        print(f"Berhasil membuat {jumlahKue} kue")

    def buangKue(self, jumlahKue) :
        if self.__kue >= jumlahKue :
            print(f"Berhasil membuang {jumlahKue} kue")
        else :
            print("Tidak bisa membuang lebih banyak kue dibandingkan dengan stok kue yang ada sekarang")

#fungsi untuk register karyawan baru
def register() :
    print("Format data: [username] [password] [nama] [umur] [role]")
    dataBaru = str(input("Input data karyawan baru: "))
    dataBaruSplit = dataBaru.split(" ")
    print(f"Karyawan {dataBaruSplit[2]} berhasil ditambahkan")

    username = dataBaruSplit[0]
    password = dataBaruSplit[1]
    nama = dataBaruSplit[2]
    umur = dataBaruSplit[3]
    role = dataBaruSplit[4]

    #memasukkan data baru pada dictionary yang tertuju pada class
    if role == "kasir" or "Kasir" :
        daftarUser[username] = Kasir(username, password, nama, umur, role)
    elif role == "janitor" or "Janitor" :
        daftarUser[username] = Janitor(username, password, nama, umur, role)
    elif role == "chef" or "Chef" :
        daftarUser[username] = Chef(username, password, nama, umur, role)

#fungsi untuk log in akun
def logIn() :
    usernameUser = str(input("Username: "))
    passwordUser = str(input("Password: "))

    if usernameUser in daftarUser and passwordUser in daftarUser :
        print(f"Selamat datang {daftarUser[usernameUser].getNama()}")
    else :
        print("username/password salah. Silahkan coba lagi")

#fungsi untuk status report toko
def statusReport() :
    user = User()
    jumlahKaryawan = len(daftarUser)
    jumlahCash = user.getPembayaran()
    jumlahKue = user.getKue()
    jumlahDeterjen = user.getDeterjen()

    print(f"""===================================
STATUS TOKO HOMURA SAAT INI
Jumlah Karyawan: {jumlahKaryawan}
Jumlah Cash: {jumlahCash}
Jumlah Kue: {jumlahKue}
Jumlah deterjen: {jumlahDeterjen}
===================================""")

daftarUser = {}

#meminta input-input pilihan pada user
while True :
    print("""Selamat datang di Sistem Manajemen Homura
    Apa yang ingin anda lakukan? (Tulis angka saja)
    1. Register karyawan baru
    2. Login
    8. Status Report
    9. Karyawan Report
    11. Exit""")
    pilihan = int(input("Pilihan: "))

    if pilihan == 11 :
        print("TERIMA KASIH TELAH MENGGUNAKAN SISTEM MANAJEMEN HOMURA")
        break

    elif pilihan == 1 :
        register()

    elif pilihan == 2 :
        userLogIn = logIn()
        if userLogIn.getRole() == "kasir" or "Kasir" :
            while True :
                print("Apa yang ingin anda lakukan? (Tulis angka saja)\n3. Terima pembayaran\n10. Logout\n")
                menu = int(input("Pilihan: "))
                if menu == 3 :
                    bayar = int(input("Jumlah pembayaran: "))
                    userLogIn.prosesPembayaran(bayar)
                elif menu == 10 :
                    print("LOGOUT BERHASIL")
                    break
                else :
                    continue

        if userLogIn.getRole() == "janitor" or "Janitor" :
            while True :
                print("Apa yang ingin anda lakukan? (Tulis angka saja)\n4. Bersihkan toko\n5. Beli deterjen\n10. Logout\n")
                menu = int(input("Pilihan: "))
                if menu == 4 :
                    userLogIn.bebersihToko()
                elif menu == 5 :
                    pembelianDeterjen = int(input("Jumlah pembelian deterjen: "))
                    userLogIn.beliDeterjen(pembelianDeterjen)
                elif menu == 10 :
                    print("LOGOUT BERHASIL")
                    break
                else :
                    continue

        if userLogIn.getRole() == "chef" or "Chef" :
            while True :
                print("Apa yang ingin anda lakukan? (Tulis angka saja)\n6. Buat kue\n7. Buang kue\n10. Logout")
                menu = int(input("Pilihan: "))
                if menu == 6 :
                    banyakKue = int(input("Buat berapa kue?: "))
                    userLogIn.buatKue(banyakKue)
                elif menu == 7 :
                    banyakKue = int(input("Buang berapa kue?: "))
                    userLogIn.buangKue(banyakKue)
                elif menu == 10 :
                    print("LOGOUT BERHASIL")
                    break
                else :
                    continue

    elif pilihan == 8 :
        statusReport()

    else :
        print("Nomor pilihan salah, silahkan coba lagi")
