#meminta input file dari user
fileInput = str(input("Masukkan nama file input: "))
fileOutput = str(input("Masukkan nama file output: "))

try :
    #membuka dan membaca file input serta membuka file output
    openInput = open(fileInput, "r")
    openOutput = open(fileOutput, "w")

    #menghitung dan mencetak jumlah baris pada file input
    sumLineInput = 0
    readInput = openInput.read()
    splitInput = readInput.split("\n")
    for i in splitInput:
        if i:
            sumLineInput += 1
    print(f"Total baris dalam file input: {sumLineInput}")

    openInput.close()

    #menentukan baris dengan jumlah angka genap untuk dicetak pada file output
    openInput = open(fileInput, "r")

    readFile = openInput.readline()
    while readFile :
        letter = ""
        number = 0
        for line in readFile :
            if line.isnumeric() :
                number += int(line)
            elif line.isalpha() :
                letter += line
            elif line == " " :
                letter += line
        readFile = openInput.readline()

        if number % 2 == 0 :
            print(letter, file = openOutput)

    openOutput.close()
    
    #menghitung dan mencetak jumlah baris pada file output
    openOutput = open(fileOutput, "r")

    sumLineOutput = 0
    readOutput = openOutput.read()
    splitOutput = readOutput.split("\n")
    for i in splitOutput:
        if i:
            sumLineOutput += 1
    print(f"Total baris dalam file output: {sumLineOutput}")

    openInput.close()
    openOutput.close()

except FileNotFoundError:
    print("Nama file yang anda masukkan tidak ditemukan :(")
