#meminta input jumlah lomba
print("""==================================
 WELCOME TO KARTI-NIAN CHALLENGE
==================================
""")
jumlahLomba = int(input("Masukkan jumlah acara yang akan dilombakan: "))
print("\n")

#meminta input sesuai iterasi jumlah lomba
dictLomba = {}
putaran = 1
while putaran < jumlahLomba+1 :
    print(f"LOMBA {putaran}")
    namaLomba = str(input("Nama lomba: "))
    juara1 = int(input("Hadiah juara 1: "))
    juara2 = int(input("Hadiah juara 2: "))
    juara3 = int(input("Haidah juara 3: "))
    print("\n")
    putaran += 1

    #memasukkan banyak lomba pada dictionary
    listJuara = []
    listJuara.append(juara1)
    listJuara.append(juara2)
    listJuara.append(juara3)
    dictLomba[namaLomba] = listJuara

print("""==================================
    DATA MATA LOMBA DAN HADIAH
==================================
""")

#mengurutkan lomba sesuai abjad dan print tiap lomba pada dictionary
sortedDictLomba = {key: value for key, value in sorted(dictLomba.items())}

for key, value in sortedDictLomba.items():
    print("Lomba", key)
    print("[Juara 1]", value[0])
    print("[Juara 2]", value[1])
    print("[Juara 3]", value[2])
    print("\n")

print("""==================================
     DATA PESERTA CHALLENGE
==================================
""")
jumlahPeserta = int(input("Masukkan jumlah peserta yang berpartisipasi: "))
print("\n")

#meminta input nama peserta
listNama = []
round = 1
while round < jumlahPeserta+1 :
    nama = str(input(f"Nama Peserta {round}: "))
    round += 1
    listNama.append(nama)

#memilah nama dengan memperhatikan case-insensitive agar tidak double
listNamaPeserta = []
listLower = []
for char in listNama :
    lower = char.lower()
    if lower not in listLower:   
        listLower.append(lower)
        listNamaPeserta.append(char)
    elif lower in listLower :
        index = listLower.index(lower)
        listNamaPeserta[index] = char

print("""
==================================
        CHALLENGE DIMULAI
==================================
""")

#meminta input nama peserta pemenang dan menjadikan dict sesuai dengan lomba
pemenang = {}
for key, value in sortedDictLomba.items():
    print(f"Lomba {key}")
    juaraSatu = str(input("Juara 1: ")).lower()
    juaraDua = str(input("Juara 2: ")).lower()
    juaraTiga = str(input("Juara 3: ")).lower()
    print("\n")

    listPemenang = []
    listPemenang.append(juaraSatu)
    listPemenang.append(juaraDua)
    listPemenang.append(juaraTiga)
    pemenang[key] = listPemenang

print("""
==================================
           FINAL RESULT
==================================""")

#mengiterasi sesuai dengan urutan nama peserta
listNamaPeserta.sort()
listLomba = [[k,v] for k,v in sortedDictLomba.items()]
for char in listNamaPeserta :
    print(char)
    charLower = char.lower()
    listJuara = []
    for char in pemenang.values() :
        if charLower in char :
            listJuara.append(char.index(charLower))
    
    #menentukan total hadiah yang didapat peserta
    lenLomba = len(sortedDictLomba)
    totalHadiah = 0
    for i in range (0, lenLomba) :
        hadiah = listLomba[i][1][listJuara[i]]
        totalHadiah+= hadiah
    print(f"Total hadiah: {totalHadiah}")

    #menentukan juara yang pernah diraih peserta
    juaraDiraih = []
    for chars in listJuara :
        if chars == 0 :
            juara = "1"
            juaraDiraih.append(juara)
        elif chars == 1 :
            juara = "2"
            juaraDiraih.append(juara)
        elif chars == 2 :
            juara = "3"
            juaraDiraih.append(juara)
        else :
            continue
    juaraDiraih.sort()
    strJuara = ", ".join(juaraDiraih)
    print(f"Juara yang pernah diraih: {strJuara}\n")
