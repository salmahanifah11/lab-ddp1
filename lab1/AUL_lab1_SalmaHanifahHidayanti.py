#meminta input karakter dari user
x = str(input("Masukkan karakter: "))

#mengubah karakter menjadi basis 10 dan rumus enkripsi fake eligma
dec_x = ord(x)
encrypted = dec_x % 7 * (dec_x ** 2) % 69 + 666
print("Fake Eligma untuk", x + ":", encrypted)

#mengubah hasil fake eligma menjadi basis 2 dan basis 16
bin_x = bin(encrypted)
hex_x = hex(encrypted)

#print hasil akhir dan slicing basis 2 serta basis 16
print("Enkripsi Fake Eligma untuk", x + ":", bin_x[2:] + hex_x[2:])
