#point awal Ei dan Raiden
ei = 100
raiden = 100

#loop menentukan pilihan user
putaran = 1
while ei > 0 and raiden > 0 :
    point = True
    point2 = True
    #while putaran >= 1 :
    putaran += 1

    while point == True :
        print("HP Ei =", ei)
        print("HP Raiden =", raiden)
        print("Putaran ke", putaran-1)
        print("---------------------------")

        #loop saat putaran kelipatan 3
        if (putaran-1) % 3 == 0:
            print("""Raiden bersiap menggunakan 'The Final Calamity'
            Apa yang ingin anda lakukan?
            1. Menyerang
            2. Menghindar
            3. Musou no Hitotachi""")
            pilihan = int(input("Masukkan pilihan anda: "))

            if pilihan == 1 :
                raiden -= 20
                ei -= 50
                print("""Ei menyerang Raiden dan mengurangi HP Raiden sebesar 20
                Raiden menyerang Ei dan mengurangi HP Ei sebesar 50
                ---------------------------""")
                point = True
                break
            elif pilihan == 2 :
                print("""Raiden menyerang namun Ei berhasil menghindar
                ---------------------------""")
                point = True
                break
            elif pilihan == 3:
                raiden -= 50
                ei -= 50
                print("""Ei menyerang Raiden dengan 'Musou no Hitotachi' dan mengurangi HP Raiden sebesar 50
                Raiden menyerang Ei dan mengurangi HP Ei sebesar 50
                ---------------------------""")
                point = False

                #loop baru setelah pilihan 3 Musou no Hitotachi
                while point2 == True :
                    putaran += 1
                    print("HP Ei =", ei) 
                    print("HP Raiden =", raiden)
                    print("Putaran ke", putaran-1)
                    print("---------------------------")

                    #jika putaran kelipatan 3    
                    if (putaran-1) % 3 == 0:
                        print("""Raiden bersiap menggunakan 'The Final Calamity'
                        Apa yang ingin anda lakukan?
                        1. Menyerang
                        2. Menghindar""")
                        pilihan = int(input("Masukkan pilihan anda: "))
                        if pilihan == 1 :
                            raiden -= 20
                            ei -= 50
                            print("""Ei menyerang Raiden dan mengurangi HP Raiden sebesar 20
                            Raiden menyerang Ei dan mengurangi HP Ei sebesar 50
                            ---------------------------""")
                            point2 = True
                        elif pilihan == 2 :
                            print("""Raiden menyerang namun Ei berhasil menghindar
                            ---------------------------""")
                            point2 = True

                    #jika putaran bukan kelipatan 3
                    else :
                        print("""Apa yang ingin anda lakukan?
                        1. Menyerang
                        2. Menghindar""")
                        pilihan = int(input("Masukkan pilihan anda: "))
                        if pilihan == 1 :
                            raiden -= 20
                            ei -= 20
                            print("""Ei menyerang Raiden dan mengurangi HP Raiden sebesar 20
                            Raiden menyerang Ei dan mengurangi HP Ei sebesar 20
                            ---------------------------""")
                            point2 = True
                        elif pilihan == 2 :
                            print("""Raiden menyerang namun Ei berhasil menghindar
                            ---------------------------""")
                            point2 = True

                    if ei <= 0 or raiden <= 0:
                        point2 = False

        #loop saat putaran bukan kelipatan 3
        else :
            print("""Apa yang ingin anda lakukan?
            1. Menyerang
            2. Menghindar
            3. Musou no Hitotachi""")
            pilihan = int(input("Masukkan pilihan anda: "))

            if pilihan == 1 :
                raiden -= 20
                ei -= 20
                print("""Ei menyerang Raiden dan mengurangi HP Raiden sebesar 20
                Raiden menyerang Ei dan mengurangi HP Ei sebesar 20
                ---------------------------""")
                point = True
                break
            elif pilihan == 2 :
                print("""Raiden menyerang namun Ei berhasil menghindar
                ---------------------------""")
                point = True
                break
            elif pilihan == 3:
                raiden -= 50
                ei -= 20
                print("""Ei menyerang Raiden dengan 'Musou no Hitotachi' dan mengurangi HP Raiden sebesar 50
                Raiden menyerang Ei dan mengurangi HP Ei sebesar 20
                ---------------------------""")
                point = False

                #loop setelah pilihan 3 Musou no Hitotachi
                while point2 == True :
                    putaran += 1
                    print("HP Ei =", ei) 
                    print("HP Raiden =", raiden)
                    print("Putaran ke", putaran-1)
                    print("---------------------------")

                    #jika putaran kelipatan 3    
                    if (putaran-1) % 3 == 0:
                        print("""Raiden bersiap menggunakan 'The Final Calamity'
                        Apa yang ingin anda lakukan?
                        1. Menyerang
                        2. Menghindar""")
                        pilihan = int(input("Masukkan pilihan anda: "))
                        if pilihan == 1 :
                            raiden -= 20
                            ei -= 50
                            print("""Ei menyerang Raiden dan mengurangi HP Raiden sebesar 20
                            Raiden menyerang Ei dan mengurangi HP Ei sebesar 50
                            ---------------------------""")
                            point2 = True
                        elif pilihan == 2 :
                            print("""Raiden menyerang namun Ei berhasil menghindar
                            ---------------------------""")
                            point2 = True

                    #jika putaran bukan kelipatan 3
                    else :
                        print("""Apa yang ingin anda lakukan?
                        1. Menyerang
                        2. Menghindar""")
                        pilihan = int(input("Masukkan pilihan anda: "))
                        if pilihan == 1 :
                            raiden -= 20
                            ei -= 20
                            print("""Ei menyerang Raiden dan mengurangi HP Raiden sebesar 20
                            Raiden menyerang Ei dan mengurangi HP Ei sebesar 20
                            ---------------------------""")
                            point2 = True
                        elif pilihan == 2 :
                            print("""Raiden menyerang namun Ei berhasil menghindar
                            ---------------------------""")
                            point2 = True

                    if ei <= 0 or raiden <= 0 :
                        point2 = False

#menentukan pemenang antara Ei dan Raiden
else :
    if ei > raiden :
        print("Ei memenangkan pertarungan")
    elif ei < raiden :
        print("Raiden memenangkan pertarungan")
    else :
        print("Pertarungan seri")
