#meminta input pesan yang diterima user
pesan = input("Masukkan pesan yang diterima: \n")

#define alphabet/angka
huruf = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
genap = "024680246802468024680246802468024680246802468024680246802468024680246802468024680246802468024680246802468024680246802468"
ganjil = "135791357913579135791357913579135791357913579135791357913579135791357913579135791357913579135791357913579135791357913579"

#mencari nilai n dan menghilangkan spasi pada pesan
findNilai = pesan.find("*")
indexNilai = pesan[findNilai+8 : findNilai+9]
nilai = 10 - int(indexNilai)

removeNilai = pesan[findNilai+1 : findNilai+9]
pesan = pesan.replace(removeNilai, "")
pesan = pesan.replace(" ","")

base = ""
for char in pesan :
    #mengambil pesan enkripsi (kapital) dan spasi ($)
    if char == char.upper() :
        base += char
    #mengambil pesan yang merupakan angka
    elif char == int :
        base += char
base = base.lower()

baseHasil = ""
for chars in base :
    #mengubah pesan enkripsi menjadi huruf/pesan yang asli
    if chars in huruf :
        findChar = huruf.rfind(chars)
        hasil = huruf[findChar - nilai]
        baseHasil += hasil
    #mengubah pesan enkripsi ($) menjadi spasi
    elif chars == "$" :
        baseHasil += chars
        baseHasil = baseHasil.replace("$"," ")
    #mengubah pesan enkripsi angka menjadi angka yang asli
    else :
        if chars in genap:
            findChar = genap.rfind(chars)
            hasil = genap[findChar - nilai]
            baseHasil += str(hasil)
        if chars in ganjil :
            findChar = ganjil.rfind(chars)
            hasil = ganjil[findChar - nilai]
            baseHasil += str(hasil)

#mencetak hasil dari pesan asli
print("Pesan asli yang diterima adalah: ")
print(baseHasil)
