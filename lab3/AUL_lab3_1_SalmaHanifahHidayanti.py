import random

#meminta input dari user
pesan = str(input("Masukkan pesan yang akan dikirim: \n"))
nilai = str(input("Pilih nilai n: "))
paragraf = "the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering, jocks fumble the pizza. the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering, jocks fumble the pizza. "

#define alphabet/angka dan lower case pesan
huruf = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
genap = "024680246802468024680246802468024680246802468024680246802468024680246802468024680246802468024680246802468024680246802468"
ganjil = "135791357913579135791357913579135791357913579135791357913579135791357913579135791357913579135791357913579135791357913579"
pesan = pesan.lower()

base = ""
for char in pesan :
    #mengubah huruf alphabet/character
    if char in huruf :
        findChar = huruf.find(char)
        hasil = huruf[findChar + int(nilai)]
        base += hasil
    #mengubah angka genap
    elif char in genap :
        findChar = genap.find(char)
        hasil = genap[findChar + int(nilai)]
        base += hasil
    #mengubah angka ganjil
    elif char in ganjil :
        findChar = ganjil.find(char)
        hasil = ganjil[findChar + int(nilai)]
        base += hasil
    #mengubah spasi
    elif char == " " :
        base += char
        base = base.replace(" ", "$")

#memasukkan hasil enkripsi pada teks paragraf
for chars in base :
    if chars in paragraf :
        upperChar = chars.upper()
        findChar = paragraf.find(chars)
        paragraf = paragraf[:findChar] + upperChar + paragraf[findChar+1:]
    if chars not in paragraf :
        paragraf = paragraf[:findChar+1] + chars + paragraf[findChar+1:]

#membuat kunci nilai dan memasukkan kunci nilai pada paragraf
kunci = 10 - int(nilai)
kunciNilai = "*2022030" + str(kunci) + "*"

charParagraf = len(paragraf)
indexRandom = random.randint(1, charParagraf)
paragraf = paragraf[:indexRandom] + kunciNilai + paragraf[indexRandom:]

#mencetak hasil pesan yang telah dienkripsi
print(paragraf)
