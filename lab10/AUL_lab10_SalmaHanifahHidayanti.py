from tkinter import *
from tkinter.filedialog import asksaveasfilename, askopenfilename
from tkinter.scrolledtext import *

class Application(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.initUI()
        self.create_buttons()
        self.create_editor()

    def initUI(self):
        self.master.title("Pacil Editor")
        self.master.geometry("545x350")

    def create_buttons(self):
        self.button1 = Button(self.master, text="Open File", command=self.load_file)
        self.button2 = Button(self.master, text="Save File", command=self.save_file)
        self.button3 = Button(self.master, text="Quit Program", command=self.quit)

        self.button1.place(x=5, y=5)
        self.button2.place(x=70, y=5)
        self.button3.place(x=130, y=5)

    def create_editor(self):
        self.edit = ScrolledText(self.master, width=65, height=25)
        self.edit.place(x=0, y=35)
        self.edit.focus()

    def load_file_event(self, event):
        self.load_file()

    def load_file(self):
        file_name = askopenfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        text_file = open(file_name, 'r', encoding="utf-8")
        result = text_file.read()
        
        self.set_text(text=result)
        text_file.close()

    def save_file_event(self, event):
        self.save_file()

    def save_file(self):
        file_name = asksaveasfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        
        text_file = open(file_name, "w", encoding="utf-8")
        result = self.get_text()
        print(result, file=text_file)
        text_file.close()

    def set_text(self, text=''):
        self.edit.delete('1.0', END)
        self.edit.insert('1.0', text)
        self.edit.mark_set(INSERT, '1.0')
        self.edit.focus()

    def get_text(self):
        return self.edit.get('1.0', END+'-1c')

    def quit(self) :
        self.master.destroy()


if __name__ == "__main__":
    root = Tk()
    app = Application(master=root)
    app.mainloop()
