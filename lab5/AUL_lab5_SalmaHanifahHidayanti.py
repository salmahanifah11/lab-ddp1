#function untuk opearasi penjumlahan
def penjumlahan() :
    #mengubah input matriks menjadi list integer
    ukuran = input("Ukuran matriks: ")
    lstUkuran = ukuran.split()
    lstUkuran.remove("x")
    lstUkuran = [int(x) for x in lstUkuran]

    #mengubah input baris matriks menjadi list integer
    lstMatriks = []
    for i in range (1, lstUkuran[0] + 1 ) :
        for j in range (1, lstUkuran[0] + 1 ) :
            matriks = input(f"Baris {j} matriks {i}: ")
            matriks = matriks.split()
            matriks = [int(x) for x in matriks]
            lstMatriks += matriks

    #menentukan hasil akhir dari penjumlahan matriks
    matriks1 = lstMatriks[: int(len(lstMatriks)/2)]
    matriks2 = lstMatriks[int(len(lstMatriks)/2) :]

    print("Hasil dari operasi: ")
    for x in range (len(matriks1)) :
        result = matriks1[x] + matriks2[x]
        print(result, end = " ")
    print()

#function untuk operasi pengurangan
def pengurangan() :
    #mengubah input matriks menjadi list integer
    ukuran = input("Ukuran matriks: ")
    lstUkuran = ukuran.split()
    lstUkuran.remove("x")
    lstUkuran = [int(x) for x in lstUkuran]

    #mengubah input baris matriks menjadi list integer
    lstMatriks = []
    for i in range (1, lstUkuran[0] + 1 ) :
        for j in range (1, lstUkuran[0] + 1 ) :
            matriks = input(f"Baris {j} matriks {i}: ")
            matriks = matriks.split()
            matriks = [int(x) for x in matriks]
            lstMatriks += matriks

    #menentukan hasil akhir dari pengurangan matriks
    matriks1 = lstMatriks[: int(len(lstMatriks)/2)]
    matriks2 = lstMatriks[int(len(lstMatriks)/2) :]

    print("Hasil dari operasi: ")
    for x in range (len(matriks1)) :
        result = matriks1[x] - matriks2[x]
        print(result, end = " ")
    print()

#function untuk operasi transpose
def transpose() :
    #mengubah input matriks menjadi list integer
    ukuran = input("Ukuran matriks: ")
    lstUkuran = ukuran.split()
    lstUkuran.remove("x")
    lstUkuran = [int(x) for x in lstUkuran]

    lstMatriks = []
    for i in range (1, lstUkuran[0] + 1) :
        matriks = input(f"Baris {i} matriks: ")
        matriks = matriks.split()
        matriks = [int(x) for x in matriks]
        lstMatriks += matriks

    #menentukan hasil transpose
    matriks1 = []
    matriks2 = []
    for x in lstMatriks :
        if lstMatriks.index(x) % 2 == 0 :
            matriks1.append(x)
        else :
            matriks2.append(x)

    print(str(matriks1))
    print(str(matriks2))

#function untuk operasi determinan
def determinan() :
    #mengubah input matriks menjadi list integer
    lstMatriks = []
    for i in range (1, 3) :
        matriks = input(f"Baris {i} matriks: ")
        matriks = matriks.split()
        matriks = [int(x) for x in matriks]
        lstMatriks += matriks
    
    #menghitung hasil akhir determinannya
    hasil = (lstMatriks[0] * lstMatriks[3]) - (lstMatriks[1] * lstMatriks[2])
    print("Hasil dari operasi: ")
    print(hasil)

#meminta input pilihan operasi pada user
putaran = True
while putaran == True :
    print("""
    Selamat datang di Matrix Calculator. Berikut adalah operasi-operasi yang
    dapat dilakukan:
    1. Penjumlahan
    2. Pengurangan
    3. Transpose
    4. Determinan
    5. Keluar
    """)
    operasi = input("Silakan pilih operasi: ")
    digitOperasi = operasi.isdigit()

    #menentukan function yang dipakai sesuai input user
    if digitOperasi == True :
        if int(operasi) == 1 :
            penjumlahan()
        elif int(operasi) == 2 :
            pengurangan()
        elif int(operasi) == 3 :
            transpose()
        elif int(operasi) == 4 :
            determinan()
        elif int(operasi) == 5 :
            print("Sampai jumpa!")
            #menyelesaikan program dengan membuat keluar dari loop
            break
        else :
            print("Pilihan tidak valid")
    else :
        print("Pilihan tidak valid")
